
    const generate = document.querySelector("#generate")
    const report = document.querySelector("#report")
    const imgContainer = document.querySelector("#image-container")
    const input = document.querySelector("#input-count")
    const clear = document.querySelector("#clear")
    const table = document.querySelector("#info-table")

    let arrayImgs = []; // array src images
    let arraySizes = []; // array img sizes
    let arrayWeights = []; // array weights img

    clear.addEventListener('click', function () {
        input.value = '';
        imgContainer.innerHTML = '';
    })

    generate.addEventListener("click", function () {
        arrayImgs = [];
        arraySizes = [];
        arrayWeights = [];
        imgContainer.innerHTML = '';

        if (input.value <= 30) {
            var addItem = function () {
                for (let i = 0; i < input.value; i++) {
                    let img = new Image();
                    let random = Math.floor(Math.random() * 1000);
                    img.src = `https://picsum.photos/200/300?image=${random}`;
                    console.log(img.sizes);

                    img.onload = () => {
                        arrayImgs.push(img);
                        arraySizes.push(
                            [img.width, img.height]
                        );
                        arrayWeights.push(
                            0
                        )
                        imgContainer.appendChild(img);
                    }
                }
            }
        } else {
            alert('Please enter the smaller number')
        }
        
        addItem();
    })

    report.addEventListener("click", function() {
        if(!document.getElementById('table-generate')) {
            let tbl = document.createElement('table');
            tbl.id = 'table-generate';
            tbl.style.width = '100%';
            let tbdy = document.createElement('tbody');

            tbl.appendChild(tbdy);
            table.appendChild(tbl);

            for (let i = 0; i < arrayImgs.length; i++) {
                let tr = document.createElement('tr');

                tbdy.appendChild(tr);

                for (let j = 0; j < 3; j++) {
                    var td = document.createElement('td');
                    tr.appendChild(td);

                    let newImg = arrayImgs[i].cloneNode(true);
                    
                    if(j === 0) {
                        td.appendChild(newImg);
                    }

                    if(j === 1) {
                        td.innerHTML += arraySizes[i][0] + 'x' + arraySizes[i][1];
                    }

                    if (j === 2) {
                        td.innerHTML += arrayWeights[i] + 'Kb';
                    }
                }
            }
        }
    })
       
    

    
